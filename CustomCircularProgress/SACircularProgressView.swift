//
//  CircularProgressView.swift
//  CustomCircularProgress
//
//  Created by S.M.Moinuddin on 12/13/17.
//  Copyright © 2017 S.M.Moinuddin. All rights reserved.
//

import UIKit

class CircularProgressView: UIView {
    
    var shouldShowIndicator = true {
        didSet{
            indicatorLayer.removeFromSuperlayer()
        }
    }
    var progress: CGFloat = 0
    let percentageLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 20)
        return label
    }()
    let secondaryLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 18)
        return label
    }()
    
    private let circleLayer = CAShapeLayer()
    private let indicatorLayer = CAShapeLayer()
    private var circularPath = UIBezierPath()
    private var radius: CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonSetup()
    }
    
    
    private func commonSetup() {
        setupShadow()
        setupCircleLayer()
        setupLables()
    }
    
    private func setupLables() {
        let boundsMidX = self.bounds.width/2 + self.bounds.origin.x
        let boundMidY =  self.bounds.height/2 + self.bounds.origin.y
        let center = CGPoint(x: boundsMidX, y: boundMidY)
        // change width height depending on bounds
        let lblView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        lblView.backgroundColor = UIColor.clear
        self.addSubview(lblView)
        lblView.center = center
        // change later for dynamic center position depending on bounds
        percentageLabel.frame = CGRect(x: 0, y: 25, width: 100, height: 30)
        secondaryLabel.frame = CGRect(x: 0, y: 45, width: 100, height: 30)
        lblView.addSubview(percentageLabel)
        lblView.addSubview(secondaryLabel)
    }
    
    
    private func setupShadow() {
        self.layer.cornerRadius = self.bounds.size.height/2
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.7
        self.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        self.layer.shadowRadius = 10
    }
    
    private func setupCircleLayer() {
        
        let boundsMidX = self.bounds.width/2 + self.bounds.origin.x
        let boundMidY =  self.bounds.height/2 + self.bounds.origin.y
        let center = CGPoint(x: boundsMidX, y: boundMidY)
        radius = (self.bounds.size.height/2 - 2) //-2 for inner circle
        
        // let's start by drawing a circle
        circularPath = UIBezierPath(arcCenter: .zero, radius: radius, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        
        circleLayer.path = circularPath.cgPath
        circleLayer.strokeColor = UIColor.magenta.cgColor
        circleLayer.lineWidth = 5
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.lineCap = kCALineCapRound
        circleLayer.position = center
        // start the the circle from 12 O'clock default is 3 O'clock
        // changing in starting angle messes with strokeEnd value...
        circleLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1)
        circleLayer.strokeEnd = 0
        
        self.layer.addSublayer(circleLayer)
        if shouldShowIndicator {
            setupIndicatorShapeLayer()
        }
    }
    
    private func setupIndicatorShapeLayer() {
        let indicatorPath = UIBezierPath(arcCenter: .zero, radius: 8, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        
        indicatorLayer.path = indicatorPath.cgPath
        indicatorLayer.strokeColor = UIColor.white.cgColor
        indicatorLayer.fillColor = UIColor.purple.cgColor
        indicatorLayer.lineWidth = 2
        indicatorLayer.strokeEnd = 1
        indicatorLayer.position = circularPath.currentPoint
        circleLayer.addSublayer(indicatorLayer)
        
    }
    
    func animate() {
        CATransaction.begin()
        CATransaction.setCompletionBlock {
            if self.progress >= 1 {
                self.indicatorLayer.removeFromSuperlayer()
            }
        }
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        
        basicAnimation.toValue = progress
        basicAnimation.duration = 0.7
        basicAnimation.fillMode = kCAFillModeForwards
        basicAnimation.isRemovedOnCompletion = false
        circleLayer.add(basicAnimation, forKey: "ProgressAnimation")
        
        if shouldShowIndicator {
            let path = UIBezierPath(arcCenter: .zero, radius: radius, startAngle: 0, endAngle: (2 * CGFloat.pi) * progress, clockwise: true)
            
            let indicatorAnimation = CAKeyframeAnimation(keyPath: "position")
            indicatorAnimation.duration = 0.7
            indicatorAnimation.path = path.cgPath
            indicatorAnimation.calculationMode = kCAAnimationPaced
            indicatorAnimation.fillMode = kCAFillModeForwards
            indicatorAnimation.isRemovedOnCompletion = false
            indicatorLayer.add(indicatorAnimation, forKey: "IndicatorPosition")
        }
        CATransaction.commit()
        
    }
}
