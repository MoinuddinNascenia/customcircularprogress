//
//  ViewController.swift
//  CustomCircularProgress
//
//  Created by S.M.Moinuddin on 23/7/18.
//  Copyright © 2018 S.M.Moinuddin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak private var progressBar: CircularProgressView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        progressBar.backgroundColor = .white
        progressBar.percentageLabel.text = "100%"
        progressBar.secondaryLabel.text = "Remaining"
        progressBar.progress = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction private func animate(_ sender: UIButton) {
        progressBar.percentageLabel.text = "30%"
        progressBar.secondaryLabel.text = "Remaining"
        progressBar.progress = 0.7
        progressBar.animate()
    }


}

